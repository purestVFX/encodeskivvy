import os, sys
import subprocess
import psutil
import time
from multiprocessing import Process, Queue
import socket
import tempfile
import json
import shutil
import argparse
from pathlib2 import Path

def ensure_dir(file_path):
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)

def sk_vailidate(args, q=None):
    infile = args["infile"]
    logpath = args["jobpath"]

    cmd = ["ffmpeg","-y","-nostats","-v","error","-i",infile,"-f","null","-"]
    if q: q.put(cmd)
    sp = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=logpath)
    if q: q.put("validation running")
    sp.wait() #wait for compleation to get return code
    stderr, stdout = sp.communicate()

    returncode = sp.returncode
        
    #write logs
    writelog(os.path.join(logpath,"stderr.txt"), stderr)
    writelog(os.path.join(logpath,"stdout.txt"), stdout)
    if q: 
        q.put("done")
        q.put(returncode)
        q.close()
        q.join_thread()

    return returncode


def sk_vp9(args, q=None):
    #print args
    progress_url = args["progress_url"]
    infile = args["infile"]
    logpath = args["jobpath"]
    startframe = args["startframe"]


    cmd = "ffmpeg -y -nostats".split() 
    cmd += ["-progress", progress_url] 
    if startframe:
        cmd += ["-framerate", "25", "-start_number", str(startframe)]
    cmd += ["-i",infile]
    #todo add gamma correct option:
    #cmd += ["-vf","eq=gamma=2.2"]
    cmd += "-c:v libvpx-vp9 -b:v 0 -crf 30".split()

    cmd_pass1 = cmd + "-pass 1 -an -f null /dev/null".split()
    cmd_pass2 = cmd + ["-pass","2",args["outfile"]]

    if q: q.put(" ".join(cmd_pass1))
    sp = subprocess.Popen(cmd_pass1, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=logpath)
    if q: q.put("pass 1 running")


    sp.wait() #wait for compleation to get return code
    stderr, stdout = sp.communicate()
    
    returncode = sp.returncode

    if not returncode:

        sp = subprocess.Popen(cmd_pass2, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=logpath)
        if q: q.put("pass 2 running")

        sp.wait() #wait for compleation to get return code
        stderr, stdout = sp.communicate()
        returncode = sp.returncode
    
    #write logs
    writelog(os.path.join(logpath,"stderr.txt"), stderr)
    writelog(os.path.join(logpath,"stdout.txt"), stdout)
    if q: 
        q.put("done")
        q.put(returncode)
        q.close()
        q.join_thread()

    return returncode



class Skivvy(object):

    def __init__(self, skivvy_root):

        self.job_que_root = os.path.join(skivvy_root,"jobs/")
        self.worker_root = os.path.join(skivvy_root,"worker/")
        self.waiting_jobs_path = os.path.join(self.job_que_root,"waiting/")
        self.running_jobs_path = os.path.join(self.job_que_root,"running/")
        self.settled_jobs_path = os.path.join(self.job_que_root,"settled/")
        self.errored_jobs_path = os.path.join(self.job_que_root,"errored/")
        
        #ensure paths exist
        ensure_dir(self.waiting_jobs_path)
        ensure_dir(self.running_jobs_path)
        ensure_dir(self.settled_jobs_path)
        ensure_dir(self.errored_jobs_path)

        self.job_data_name = "jobdetails.json"
        self.running_procs = {}
        self.cpu_load = None
        self.network_load = None
        self.worker_status_dir = ""

    def write_worker_status(self):
        hostname = socket.gethostname().split(".")[0]
        pid = os.getpid()

        #create worker_status_dir
        self.worker_status_dir = os.path.join(self.worker_root, hostname+"/")
        ensure_dir(self.worker_status_dir)
        status= {"cpu": self.cpu_load, "net":self.network_load, "procs": len(self.running_procs), "pid": pid}
        with open(os.path.join(self.worker_status_dir, "status.json"), 'w') as fh:
            json.dump(status, fh)
        Path(os.path.join(self.worker_status_dir,str(pid))).touch()
    
    def check_for_stop(self):
        stop_path =  os.path.join(self.worker_status_dir,"stop")
        if os.path.exists(stop_path):
            return True
        return False

    def update_worker_load(self):
        self.network_load = net_usage()
        self.cpu_load = psutil.cpu_percent()

    def count_waiting_jobs(self):
        return len(os.listdir(self.waiting_jobs_path))

    def add_job_to_que(self, infile, outfile=None, startframe=None, jobtype="vp9", priority="B", jobname="unnamed_encode_job"):
        timestr = str(int(time.time()))
        jobid_prefix = "sk_"+priority+"_"+timestr+"_"
        jobpath = tempfile.mkdtemp(dir=self.waiting_jobs_path, prefix=jobid_prefix)
        
        jobDetails = {"infile":infile, "outfile":outfile, "jobtype":jobtype, "jobname":jobname, "startframe":startframe}
        with open(os.path.join(jobpath, self.job_data_name), 'w') as fh:
            json.dump(jobDetails, fh)

        open(os.path.join(jobpath,"ready"),"w").close() #mark job as ready to start

    def read_job_data(self, jobpath):
        with open(os.path.join(jobpath, self.job_data_name)) as json_file:
            return json.load(json_file)

    def take_next_job(self):
        job_ids = os.listdir(self.waiting_jobs_path)
        job_ids.sort()
        for job_id in job_ids:
            jobpath = os.path.join(self.waiting_jobs_path, job_id)
            if self.validate_job_id(job_id) and os.path.isdir(jobpath):
                readyfile = os.path.join(jobpath,"ready")
                if os.path.exists(readyfile):
                    try:
                        os.mkdir(os.path.join(jobpath,"taken"))  # ensure only one process takes the job (mkdir fails if the directory exists)
                    except exception:
                        print "an error, job probably already taken!!!!!!!!!!!!!!!!!!"
                        #pass #todo - properly catch the right exception:     OSError: [Errno 17] File exists:
                    else:
                        os.remove(readyfile)
                        running_jobpath = os.path.join(self.running_jobs_path, job_id)
                        shutil.copytree(jobpath, running_jobpath) # now move the whole job into the "running" dir
                        shutil.rmtree(jobpath)
                        return job_id, running_jobpath
        return

    def run_next_job(self):
        next_job = self.take_next_job()
        if next_job:
            job_id, jobpath = next_job
            job_data = self.read_job_data(jobpath)
            job_data["progress_url"] = "file:/"+jobpath+"/progress.log"
            job_data["jobpath"] = jobpath
            print "launching ", job_id
            if job_data["jobtype"] == "vp9":
                self.new_proc(job_id, sk_vp9, job_data)
            elif job_data["jobtype"] == "validate":
                self.new_proc(job_id, sk_vailidate, job_data)

    def validate_job_id(self, job_id):
        #todo : better validation? (avoid a random file (eg: apple mac disk poo) breaking everthing)
        if job_id.startswith("sk_"):
            return True
        return False

    def new_proc(self, job_id, target, args_dict):
        q = Queue()
        p = Process(target=target, args=(args_dict, q))
        p.start()
        self.running_procs[job_id] = {"p":p, "q":q, "args":args_dict}

    def end_proc(self, job_id):
        job = self.running_procs[job_id]
        job["q"].close()
        job["p"].join() #end the process
        if job["returncode"]:
            new_jobpath = os.path.join(self.errored_jobs_path, job_id)
        else:
            new_jobpath = os.path.join(self.settled_jobs_path, job_id)
        
        shutil.copytree(job["args"]["jobpath"], new_jobpath)
        shutil.rmtree(job["args"]["jobpath"])

        del self.running_procs[job_id]

    def check_procs(self):
        for job_id in self.running_procs.keys():
            q = self.running_procs[job_id]["q"]
            job_name = self.running_procs[job_id]["args"]["jobname"]
            while not q.empty():
                data = q.get()
                print "# ", job_id, job_name, " : ", data
                sys.stdout.flush()
                if data == "done":   # process has finished, cleanup time
                    returncode = q.get()
                    print "# ", job_id, job_name, " : ", "returncode : ", returncode
                    sys.stdout.flush()
                    self.running_procs[job_id]["returncode"]=returncode
                    self.end_proc(job_id)



def net_usage(inf = "eth0", n=10):   #change the inf variable according to the interface
    "get the avarage network traffic over n seconds"
    net_stat = psutil.net_io_counters(pernic=True, nowrap=True)[inf]
    net_in_1 = net_stat.bytes_recv
    net_out_1 = net_stat.bytes_sent
    time.sleep(n)
    net_stat = psutil.net_io_counters(pernic=True, nowrap=True)[inf]
    net_in_2 = net_stat.bytes_recv
    net_out_2 = net_stat.bytes_sent

    net_in = round((net_in_2 - net_in_1) / (131072.0*n), 3)
    net_out = round((net_out_2 - net_out_1) / (131072.0*n), 3)

    return net_in+net_out


def writelog(path, text):
    if text:
        with open(path, 'w') as fh:
            fh.write(text)



def test_create_some_jobs(skiv):
    in_frames = '/mnt/apps/TEMP_PROJECT_ROOT/OKIDO_2_caches/gaffer_test/renders/cycles_v003/beauty/beauty.%04d.exr'

    small_mov = '/home/rupertt/encoding_tests/v003/ok_cmp_he09_0600_v003.mov'
    big_mov = '/home/rupertt/okipedia/season_two_episodes/media/S2_Ep26--Zoom_Has_a_Dream_1280.mp4'

    tmp_out = "/home/rupertt/encoding_tests/"

    skiv.add_job_to_que(in_frames, tmp_out+"from_frames.mp4", startframe=1001, jobname="frame job")

    
    skiv.add_job_to_que(small_mov, tmp_out+"test1.mp4", jobname="1st_job_added__")
    time.sleep(1)
    #skiv.add_job_to_que(small_mov, tmp_out+"test2.mp4", jobname="2nd_job_added__")
    #time.sleep(1)
    #skiv.add_job_to_que(small_mov, tmp_out+"test3.mp4", jobname="3rd_hi_priority", priority="A")


def test():
    skiv = Skivvy("/mnt/apps/TEMP_PROJECT_ROOT/skivvy/")
    test_create_some_jobs(skiv)

    stop = False

    while (not stop and skiv.count_waiting_jobs()) or len(skiv.running_procs):
        
        skiv.update_worker_load()
        skiv.write_worker_status()
        stop = skiv.check_for_stop()

        if (not stop and float(skiv.cpu_load)<80 and 
           (skiv.network_load < 500) and len(skiv.running_procs) < 3):
            skiv.run_next_job()

        skiv.check_procs()

    print "all processes closed"
    


def daemon_test():
    import daemon
    with daemon.DaemonContext():
        test()
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='encoding skivey')
    parser.add_argument("--test", action="store_true")
    parser.add_argument("--testd", action="store_true")
    parser.add_argument("--run", action="store_true")
    parser.add_argument("--netuse", action="store_true")
    args = vars(parser.parse_args())

    if args["test"]:
        test()
    if args["testd"]:
        daemon_test()
    if args["run"]:
        run_skivvey()
    if args["netuse"]:
        print net_usage()

    