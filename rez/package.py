# -*- coding: utf-8 -*-

name = 'encodeSkivvy'

version = '0.0.1'

description = 'encodeSkivvy repo'

variants = [['python-2.7']]

requires = [
    "psutil",
    "python_daemon",
    "ffmpeg",
    "pathlib2"
]

authors = ['Rupert Thorpe']

build_command = 'python {root}/rezbuild.py {install}'

def commands():
    env.PYTHONPATH.append('{root}/encodeSkivvy')