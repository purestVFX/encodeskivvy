from os.path import join, exists, dirname
from shutil import rmtree, copytree


def build(source_path, build_path, install_path, targets):

    def _install():
        src = join(source_path, "../")
        dest = join(install_path, 'encodeSkivvy')

        if exists(dest):
            rmtree(dest)

        copytree(src, dest)
        rmtree(dirname(build_path))


    if 'install' in (targets or []):
        _install()

if __name__ == '__main__':
    import os, sys
    build(
        source_path=os.environ['REZ_BUILD_SOURCE_PATH'],
        build_path=os.environ['REZ_BUILD_PATH'],
        install_path=os.environ['REZ_BUILD_INSTALL_PATH'],
        targets=sys.argv[1:]
    )