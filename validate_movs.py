import encodeSkivvy
import os

skivvyroot = "/mnt/apps/TEMP_PROJECT_ROOT/skivvy/"
skivvy = encodeSkivvy.Skivvy(skivvyroot)


#validation_dir = "/home/rupertt/encoding_tests/"
validation_dir ="/mnt/okido/MESSY_GOES_TO_OKIDO/Returned from DHX/"

for root, dirs, files in os.walk(validation_dir):
    for file in files:
        if file.endswith(".mov") or file.endswith(".mp4"):
             movpath = os.path.join(root, file)
             print movpath
             skivvy.add_job_to_que(infile = movpath, jobtype="validate")
